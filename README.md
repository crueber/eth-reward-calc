# Ethereum Rewards Calculator

There were no obvious rewards calculators that I found anywhere, so I decided to throw this out there. Because the rewards are calculated and not available anywhere directly in the RPC layers, I needed an easy way to calculate this data. Feel free to send PRs as this information moves forward.

All results are expressed in Wei, base10, and in string format.

# Examples

Block Reward:

```js
const calc = require('eth-reward-calc');

calc.calculateBlockReward(4000000, 1)
// '5156250000000000000'
```

Uncle Reward:

```js
calc.calculateUncleReward(4321, 4319)
// '3750000000000000000'
```

Base Reward:

```js
calc.getBaseReward(4000000)
// '5000000000000000000'
```

# MIT License

Copyright 2018 &copy; Christopher WJ Rueber

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

