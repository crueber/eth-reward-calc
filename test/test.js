
const { expect } = require('chai');

const calc = require('..');

describe('block sections', () => {
  it('should recognize frontier', () => {
    expect(calc.isBlockFrontier(18)).to.be.true;
    expect(calc.isBlockFrontier(4999999)).to.be.false;
  });
  it('should recognize byzantium', () => {
    expect(calc.isBlockByzantium(18)).to.be.false;
    expect(calc.isBlockByzantium(5000000)).to.be.true;
  });
  it('should hate on negatives', () => {
    expect(calc.isBlockByzantium(-1)).to.be.false;
    expect(calc.isBlockFrontier(-1)).to.be.false;
  })
})

describe('block reward', () => {
  it('should calculate constantinople reward', () => {
    expect(calc.calculateBlockReward(7300000, 0)).to.equal('2000000000000000000');
  });
  it('should calculate base reward', () => {
    expect(calc.calculateBlockReward(4000000, 0)).to.equal('5000000000000000000');
  });
  it('should calculate uncles too', () => {
    expect(calc.calculateBlockReward(4000000, 1)).to.equal('5156250000000000000');
  });
})

describe('uncle reward', () => {
  it('should calculate uncle reward', () => {
    expect(calc.calculateUncleReward(4321, 4321)).to.equal('5000000000000000000');
    expect(calc.calculateUncleReward(4321, 4320)).to.equal('4375000000000000000');
    expect(calc.calculateUncleReward(4321, 4319)).to.equal('3750000000000000000');
    expect(calc.calculateUncleReward(4321, 4318)).to.equal('3125000000000000000');
  })
});
