const BN = require('bignumber.js');

const rewards = {
  frontier: new BN(5e+18),
  byzantium: new BN(3e+18),
  constantinople: new BN(2e+18)
};

const isBlockFrontier = (blockNumber) => { return blockNumber >= 0 && blockNumber < 4370000; };
const isBlockByzantium = (blockNumber) => { return blockNumber >= 4370000 && blockNumber < 7280000; };
const isBlockConstantinople = (blockNumber) => { return blockNumber >= 7280000; };

const getBaseReward = (blockNumber) => {
  if (isBlockFrontier(blockNumber)) return rewards.frontier;
  if (isBlockByzantium(blockNumber)) return rewards.byzantium;
  if (isBlockConstantinople(blockNumber)) return rewards.constantinople;
  return false;
};

const calculateUncleReward = (blockNumber, uncleBlockNumber) => {
  if (!blockNumber || !uncleBlockNumber) throw new Error('Uncle reward requires blockNumber and uncleBlockNumber');
  const baseReward = getBaseReward(blockNumber);
  return `${new BN((uncleBlockNumber + 8) - blockNumber).multipliedBy(baseReward).dividedBy(new BN(8))}`;
};

const calculateBlockReward = (blockNumber, numberOfUncles) => {
  if (typeof blockNumber !== 'number') throw new Error('Block number is required to calculate block reward');
  if (typeof numberOfUncles !== 'number') throw new Error('Number of uncles is required to calculate block reward');

  const baseReward = getBaseReward(blockNumber);
  if (typeof blockNumber !== 'number') throw new Error('Block number not in expected ranges for rewards.')

  const uncleInclusionReward = baseReward.dividedBy(32).multipliedBy(numberOfUncles);
  return `${baseReward.plus(uncleInclusionReward)}`;
};

module.exports = {
  isBlockConstantinople,
  isBlockByzantium,
  isBlockFrontier,
  getBaseReward,
  calculateBlockReward,
  calculateUncleReward
};
